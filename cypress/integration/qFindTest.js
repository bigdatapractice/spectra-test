describe('Should load page', function () {
    it('Page Loading', function () {
    cy.visit('login?app=ROLE_QFIND');
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
      });
      Cypress.Cookies.defaults({
        whitelist: "JSESSIONID"
      })
      cy.wait(2000)

    })
});

/*describe('Check in-valid user credentials', function () {
    it('should add in-valid username and password', function () {
    cy.get('#username')
    .type('dem')
    .should('have.value', 'dem')
    cy.get('#password')
    .type('dem')
    .should('have.value', 'dem')
    cy.get('[type="submit"]').click() ;
    cy.wait(3000)
    })
})*/

describe('Log-In Verification', function () {
    it('should add valid username and password', function () {
    cy.visit('login?app=ROLE_QFIND');
    cy.get('#username')
    .type('saurabh')
    .should('have.value', 'saurabh')
    cy.wait(3000)
    cy.get('#password')
    .type('saurabh@rl$')
    .should('have.value', 'saurabh@rl$')
    cy.wait(3000)
    cy.get('[type="submit"]').click() ;
    cy.wait(10000)
    })
})

describe('Landing page Verification ',function(){
    it('should contain all images', function () {
        cy.visit("index#IntegraNew")
        cy.wait(30000);
        let count=0;
       // cy.get('#clinicalSpecialityItemsDiv').find('img').each($el => {
        //cy.get($el).should('have.attr', 'src')
       // count++;
       // });
       cy.get('#clinicalSpecialityItemsDiv').children().children().children().should(($image) => {
        expect($image[0].naturalWidth).to.be.greaterThan(0)  
       });
       //cy.get('#clinicalSpecialityItemsDiv').children().children().children()[0].naturalWidth.should('be.gt', 0)
       cy.get('#clinicalSpecialityItemsDiv')
       .children().children().children()
       .each(($img) => {
        if($img[0].naturalWidth>0){
       // "naturalWidth" and "naturalHeight" are set when the image loads
       expect($img[0].naturalWidth).to.be.greaterThan(0)
        }
       count++;
      })
        cy.wait(10000);
      
        //cy.get('#clinicalSpecialityItemsDiv')
        //cy.get('#clinicalSpecialityItemsDiv').find('img').should('have.attr', 'src')//.should('include','https://spectrafiles.relevancelab.com/integra/qfind_images/franchise/Acute instruments')
        
    })
})

describe('Search Criteria & Checking All tab ',function(){
    it('Title click for all tab', function () {
        cy.visit("index#IntegraNew")
        cy.wait(30000)  
            cy.get("#searchForm").type('*');
            cy.get("#search").click({force:true});
            cy.wait(3000)
            cy.get('#search-result')
            .then((element) => {
            expect(element.text()).to.equal('Showing 210,035 results "*"');
            });
            cy.wait(3000)
            //cy.get('.All-result-count')
           // expect(0).to.be.greaterThan(0);
           cy.get('.All-result-count')
            .then((element) => {
            expect(parseInt(element.text())).to.be.greaterThan(0);
            });
            cy.wait(20000)
           // cy.get('#searchTabContent').find('img').each($el => {
              //  cy.get($el).should('have.attr', 'src')
            //});
            cy.get('#searchTabContent').children().children().children().children().children().children().children().should(($image) => {
                expect($image[0].naturalWidth).to.be.greaterThan(0)  
            });
           cy.get('#searchTabContent')
            .children().children().children().children().children().children().children()
            .each(($img) => {
            // "naturalWidth" and "naturalHeight" are set when the image loads
            if($img[0].naturalWidth>0){
            expect($img[0].naturalWidth).to.be.greaterThan(0)
            }
            });

            cy.get(".search-title").first().click({force:true});
            cy.wait(1000)
            cy.get("#wid-id-header");
            cy.get("#searchBack").click({force:true});
            cy.wait(10000);
            
        })
})

describe('Checking Other tabs ',function(){
        it('Title click for remaining tab', function () {
        cy.visit("index#IntegraNew")
        cy.wait(30000)
         cy.get("#searchForm").type('*');
            cy.get("#search").click({force:true});
            cy.wait(3000)
            cy.get('#search-result')
            .then((element) => {
            expect(element.text()).to.equal('Showing 210,035 results "*"');
            });
            cy.wait(3000)
            cy.get(".searchTab-1").click({force:true});
            cy.wait(10000)
            cy.get('.Product_Catalog-result-count')
            .then((element) => {
            expect(parseInt(element.text())).to.be.greaterThan(0);
            });
            cy.wait(30000)
           // cy.get('#searchTabContent').find('img').each($el => {
              //  cy.get($el).should('have.attr', 'src')
            //});

            cy.get('#searchTabContent').find('.search-results').children().should(($image) => {
                expect($image[7].naturalWidth).to.be.greaterThan(0)  
            });
            cy.get('#searchTabContent')
            .find('.search-results').children()
            .each(($img) => {
            if($img[0].naturalWidth>0){
             expect($img[0].naturalWidth).to.be.greaterThan(0)
            }
            });
            cy.get(".valid-document").first().click({force:true});
            cy.wait(10000)
            cy.get("#searchBack").click({force:true});
            cy.get(".searchTab-2").click({force:true});
            cy.wait(10000)
            cy.get('.Agile-result-count')
            .then((element) => {
            expect(parseInt(element.text())).to.be.greaterThan(0);
            });
            cy.get(".valid-document").first().click({force:true});
            cy.wait(10000)
            cy.get("#wid-id-header");
            cy.get("#searchBack").click({force:true});
            cy.get(".searchTab-3").click({force:true});
            cy.wait(10000)
            cy.get('.Item_Master-result-count')
            .then((element) => {
            expect(parseInt(element.text())).to.be.greaterThan(0);
            });
            cy.get(".valid-document").first().click({force:true});
            cy.wait(10000)
            cy.get("#wid-id-header");
            cy.get("#searchBack").click({force:true});
            cy.get(".searchTab-4").click({force:true});
            cy.wait(10000)
            cy.get('.Labels-result-count')
            .then((element) => {
            expect(parseInt(element.text())).to.be.greaterThan(0);
            });
            cy.get(".valid-document").first().click({force:true});
            cy.wait(10000)
            cy.get("#wid-id-header");
            cy.get("#searchBack").click({force:true});
            cy.get(".searchTab-5").click({force:true});
            cy.wait(10000)
            cy.get('.Label_Editable-result-count')
            .then((element) => {
            expect(parseInt(element.text())).to.be.greaterThan(0);
            });
            cy.get(".valid-document").first().click({force:true});
            cy.wait(10000)
            cy.get("#wid-id-header");
            cy.get("#searchBack").click({force:true});
            cy.get(".searchTab-6").click({force:true});
            cy.wait(10000)
            cy.get('.Catalog_Editable-result-count')
            .then((element) => {
            expect(parseInt(element.text())).to.be.greaterThan(0);
            });
            cy.wait(10000)
           //cy.get('#searchTabContent').find('img').each($el => {
              //  cy.get($el).should('have.attr', 'src')
            //});
            cy.get('#searchTabContent')
            .find('.search-results').children()
            .each(($img) => {
            if($img[0].naturalWidth>0){
             expect($img[0].naturalWidth).to.be.greaterThan(0)
            }
            });
            cy.get(".valid-document").first().click({force:true});
            cy.wait(10000)
            cy.get("#wid-id-header");
            cy.get("#searchBack").click({force:true});
            cy.get(".searchTab-0").click({force:true});
            cy.wait(10000)
           // cy.get('.All-result-count')
           // expect(193162).to.be.greaterThan(0);
            //cy.wait(5000)
            cy.get('.All-result-count')
            .then((element) => {
            expect(parseInt(element.text())).to.be.greaterThan(0);
            });
           // cy.get(".search-results").first()
            //cy.get(".btn-success").first().click()

           // cy.wait(2000)
            //cy.get("#searchBack").click();
        })
})

/*export const getIframeBody = (locator) => {
    // get the iframe > document > body
    // and retry until the body element is not empty
    cy.wait(5000)
    return cy
      .get(locator)
      .its('0.contentDocument.body').should('not.be.empty')
      // wraps "body" DOM element to allow
      .then(cy.wrap)
  }*/

  const getIframeDocument = () => {
    return cy
    .get('iframe[id="iframe_a"]')
    // Cypress yields jQuery element, which has the real
    // DOM element under property "0".
    // From the real DOM iframe element we can get
    // the "document" element, it is stored in "contentDocument" property
    // Cypress "its" command can access deep properties using dot notation
    // https://on.cypress.io/its
    .its('0.contentDocument').should('exist')
    }
    
    const getIframeBody = () => {
    // get the document
    return getIframeDocument()
    // automatically retries until body is loaded
    .its('body').should('not.be.undefined')
    // wraps "body" DOM element to allow
    // chaining more Cypress commands, like ".find(...)"
    .then(cy.wrap)
    }

describe('Checking for Pdf ', function () {
    it('PDF check', function () {
        cy.visit("index#IntegraNew")
        cy.wait(30000)
         cy.get("#searchForm").type('*');
            cy.get("#search").click({force:true});
            cy.wait(3000)
            cy.get('#searchTabContent')
            cy.get('.search-results')
            cy.get(".btn-success").eq(3).click({force:true})
            cy.get(".modal-body")
            cy.get(".poDocView").find(".search-title").click({force:true, multiple: true })
            cy.wait(15000)
         
    
           // cy.wait('@getAccount').its('status').should('eq', 200)
          //let iframeStripe = 'iframe[name="iframe_a"]'
          // getIframeBody().find("#content")
          getIframeBody().then((xhr) => { 
            //expect(xhr[0].baseURI).to.equal('https://sandbox.relevancelab.com/SPECTRA/index#IntegraNew');
          }).find("embed")


    })
})